const express = require('express');
const PORT = 4000;
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();

//mongoose.set('useCreateIndex', true);

//Middleware
app.use(cors());
app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());
app.use(morgan("dev"));
app.get("/", (req, res) => {
  console.log("Hello MEVN Soldier");
  res.send('Hello, it is work')
});
// const userRoutes = require("./routes/user"); //bring in our user routes
// const posts = require('./routes/api/post');
// const auth = require('./routes/api/auth');
// const products = require('./routes/api/products');
// const feedback = require('./routes/api/feedback');
const userFormDates = require('./routes/api/userFormDates')
app.use('/api/formDates', userFormDates);
// app.use('/api/auth', auth);
// app.use('/api/products', products);
// app.use("/user", userRoutes);
// app.use('/api/feedback', feedback)
app.listen(PORT, () => {
  console.log(`App is running on ${PORT}`);
});
