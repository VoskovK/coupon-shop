const express = require('express');
const mongodb = require('mongodb');

const router = express.Router();

//Get UserDates
router.get('/', async (req, res) => {
  const userDates = await loadUserDatesCollection();
  res.send(await userDates.find({}).toArray());
})
//Add UserDates
router.post('/', async (req, res) => {
  const userDates = await loadUserDatesCollection();
  await userDates.insertOne({
    fullname: req.body.fullname,
    emial: req.body.email,
    discountCode: req.body.discountCode,
    sum: req.body.sum,
    typeCode: req.body.typeCode,
    createdAt: new Date(),
  });
  res.status(201).send();
})
//Delete UserDates

async function loadUserDatesCollection() {
  const client = await mongodb.MongoClient.connect('mongodb://localhost:27017/authapp', {
    useNewUrlParser: true
  });

  return client.db('couponShop').collection('userFormDates');
}

module.exports = router;
