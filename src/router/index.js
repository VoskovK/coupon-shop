import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route
  // which is lazy-loaded when the route is visited.
  {
    path: '/',
    name: 'home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/coupon/:id',
    name: 'couponsItem',
    component: () => import('@/components/Coupons/CouponsItem.vue')
  },
  {
    path: '/lead-in',
    name: 'leadIn',
    component: () => import('@/views/leadIn/')
  },
  {
    path: '/lead-out',
    name: 'leadOut',
    component: () => import('@/views/leadOut')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
