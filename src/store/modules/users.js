import axios from 'axios'
import { baseAPI_URL } from '../../constants';

export default {
  state: {
    userFormList: JSON.parse(localStorage.getItem('userDates') || '[]')
  },
  actions: {
    sendData({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        axios.post(baseAPI_URL + '/api/formDates', userInfo)
          .then(() => {
            console.log("action userFormDates: ", userInfo)
            commit('SEND_DATA', userInfo);

            resolve(userInfo)
          }).catch(reject)
      })
    }
  },
  mutations: {
    SEND_DATA(state, userInfo) {
      state.userFormList = userInfo
    }
  },
  getters: {},

  namespaced: true
}